### TODO Activity 

Developed By Ravi Jain using Javascript, HTML and CSS

### Download the project 

Clone the project by using git clone https://ravijain2390@bitbucket.org/ravijain2390/todo_activity.git 

### Run the Project

Run the project by using todo_activity.html file

### Unit Tests

Unit Test can be run using unit_test.html file
